//
//  JSON+iSage.h
//  iSage
//
//  Created by Daniel Bell on 2014-05-22.
//  Copyright (c) 2014 Sagemath. All rights reserved.
//

// Adapters to NSJSONSerializer using the JSONKit interface

@protocol JSONKitAdapterSerializing <NSObject>
- (NSString *)JSONString;
@end

@interface NSString (JSONKitAdapterCategories) <JSONKitAdapterSerializing>
- (NSString *)JSONString;
@end





