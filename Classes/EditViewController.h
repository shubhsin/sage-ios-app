//
//  EditViewController.h
//  iSage
//
//  Created by Ivan Andrus on 4/29/11.
//  Copyright 2011 Ivan Andrus. All rights reserved.
//
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

#import <UIKit/UIKit.h>
#import "ShareButtonDelegate.h"
#import "EquationSelectionDelegate.h"

@interface EditViewController : UIViewController <UITextViewDelegate,
                                                  UIGestureRecognizerDelegate,
                                                  UIActionSheetDelegate,
                                                  UISplitViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *evaluateAndShareSegmentedController;
@property (strong, nonatomic) IBOutlet UITextView *inputView;
@property (strong, nonatomic) IBOutlet UIWebView *outputView;
@property (strong, nonatomic) UITextView *activeField;
@property (nonatomic, strong) NSManagedObject *selectedObject;
@property (strong, nonatomic) UIBarButtonItem *blankEquationButton;
@property (nonatomic, strong) id delegate;
@property (strong, nonatomic) UIActionSheet *actionSheet;
@property (strong, nonatomic) UILongPressGestureRecognizer *longPress;

@property (nonatomic, assign, getter=isFullScreenOutput) BOOL FullScreenOutput;
@property (nonatomic, assign, getter=isLoaded) BOOL Loaded;
@property (nonatomic, assign) BOOL shouldLoad;

@property (nonatomic, weak) IBOutlet UIView *keyboardAccessory;

- (void)registerForKeyboardNotifications;
- (void)updateImageSize;
- (void)updateOutput;
- (void)saveOutput:(NSString*)status;
-(void)updateView;
//- (void)updateToolbarForEvaluation:(BOOL)shouldEval;

// Keyboard Accessory Actions
- (IBAction)insertDelimiters:(id)sender;
- (IBAction)insertTab:(id)sender;
- (IBAction)insertCharacter:(id)sender;
- (IBAction)hideKeyboard:(id)sender;

// Button actions (added programattically, but they could be IB actions)
- (IBAction)eval:(id)sender;
- (IBAction)shareCell:(id)sender;
- (void)selectLanguage:(id)sender;
- (IBAction)newCell:(id)sender;
- (void)newCellWithDefault:(AlephCell*)str;


// Gesture actions (added programattically, but they could be IB actions)
- (IBAction)swipe:(id)sender;
- (IBAction)doubleSwipe:(id)sender;
- (void)twoTaps:(id)sender;


@end
