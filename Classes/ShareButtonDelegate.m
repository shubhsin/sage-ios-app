//
//  ShareButtonDelegate.m
//  iSage
//
//  Created by Ivan Andrus on 10/27/12.
//  Copyright (c) 2012 Sagemath. All rights reserved.
//
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

#import "ShareButtonDelegate.h"

@implementation ShareButtonDelegate

@synthesize cell;

-(ShareButtonDelegate*)init
{
    self = [super init];
    if (self) {
        isPhone = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
    }
    return self;
}

-(void)displayShareSheet:(UIViewController*)controller forCell:(AlephCell*)newCell fromWhence:(id)origin
{
    if (viewController) {
        NSLog(@"I shouldn't have to release this controller.");
    }
    if (cell) {
        NSLog(@"I shouldn't have to release this cell.");
    }
    cell = newCell;
    viewController = controller;

    if (!actionSheet) {

        actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share"
                                                  delegate:self
                                         cancelButtonTitle:(isPhone ? @"Cancel" : nil)
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"Duplicate",
                       @"Copy Output",
                       @"Copy Input and Output",
                       // http://stackoverflow.com/questions/5349139/how-to-send-an-email-through-ios-simulator
                       ([MFMailComposeViewController canSendMail] ? @"Send Email" : nil),
                       ([MFMailComposeViewController canSendMail] ? @"Report Bug" : nil),
                       nil];
    }

    if ( isPhone ) {
        [actionSheet showFromToolbar:viewController.navigationController.toolbar];
    } else if ( [origin isKindOfClass:[UIBarButtonItem class]] ) {
        [actionSheet showInView:controller.view.window];
    } else if ( [origin isKindOfClass:[UIView class]] ) {
        UIView *superview = [origin superview];
        [actionSheet showFromRect:[superview convertRect:[origin bounds] fromView:origin]
                           inView:superview animated:YES];
    }
}

#pragma mark -
#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)as clickedButtonAtIndex:(NSInteger)buttonIndex
{
    BOOL releaseViewController = YES;
    if (cell != nil && buttonIndex != [as cancelButtonIndex]) {
        UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
        switch (buttonIndex) {

            case 0: // duplicate
                [viewController performSelector:@selector(newCellWithDefault:)
                                     withObject:cell
                                     afterDelay:0];
                break;


            case 1: // copy output
                pasteboard.string = cell.output;
                if (cell.image) {
                    [pasteboard addItems:[NSArray arrayWithObject:
                                          [NSDictionary dictionaryWithObject:cell.image
                                                                      forKey:@"public.png"]]];
                }
                break;

            case 2: // copy both
                pasteboard.string = [NSString stringWithFormat:@"%@\n\n%@",
                                     cell.input,
                                     cell.output];
                if (cell.image) {
                    [pasteboard addItems:[NSArray arrayWithObject:
                                          [NSDictionary dictionaryWithObject:cell.image
                                                                      forKey:@"public.png"]]];
                }
                break;

            case 3:  // email
            case 4:  // report bug
            {
                MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
                picker.mailComposeDelegate = self;

                if (buttonIndex == 4) { // Report Bug
                    [picker setSubject:[NSString stringWithFormat:@"Bug in Sage on my %@",
                                        [[UIDevice currentDevice] name]]];
                    [picker setToRecipients:[NSArray arrayWithObject:@"sage-support@googlegroups.com"]];
                } else {
                    [picker setSubject:[NSString stringWithFormat:@"Sage on my %@",
                                        [[UIDevice currentDevice] name]]];
                }

                NSString *emailBody =[NSString stringWithFormat:@"<html><style>.sagecell_messages {display:%@;}</style><pre>%@</pre>\n\n<div>%@</div></html>",
                                      ((buttonIndex == 4) ? @"block" : @"none"),
                                      cell.input,
                                      cell.html_output];
                [picker setMessageBody:emailBody isHTML:YES];
                [viewController presentViewController:picker animated:YES completion:nil];
                releaseViewController = NO;
            }
                break;

            default:
                break;
        }
    }
    cell = nil;
    // We might still need the viewController for email composer
    if (releaseViewController) {
        viewController = nil;
    }
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error
{
    // Notifies users about errors associated with the interface
    switch (result) {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail canceled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed:
            NSLog(@"Mail failed: %@",[error localizedDescription]);
            break;
        default:
            NSLog(@"Mail not sent");
            break;
    }
    [viewController dismissViewControllerAnimated:YES completion:nil];
    viewController = nil;
}

@end
