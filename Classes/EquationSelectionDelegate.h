//
//  EquationSelectionDelegate.h
//  iSage
//
//  Created by Daniel Bell on 2014-07-20.
//  Copyright (c) 2014 Sagemath. All rights reserved.
//

#import <Foundation/Foundation.h>
@class Equation;
@protocol EquationSelectionDelegate <NSObject>
@required
-(void)selectedEquation:(NSManagedObject *)newEquation;
@end
