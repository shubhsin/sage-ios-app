//
//  RootViewController.m
//  iSage
//
//  Created by Ivan Andrus on 4/29/11.
//  Copyright 2011 Ivan Andrus. All rights reserved.
//
/*
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this
 list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 The views and conclusions contained in the software and documentation are those
 of the authors and should not be interpreted as representing official policies,
 either expressed or implied, of the FreeBSD Project.
 */

#import <UIKit/UITableView.h>
#import "iSageAppDelegate.h"
#import "RootViewController.h"
#import "AlephCell.h"
#import "EditViewController.h"
#import "SwipeableCell.h"


@interface RootViewController ()<SwipeableCellDelegate>{
    NSMutableArray *_objects;
}
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@property (nonatomic, strong) NSMutableArray *cellsCurrentlyEditing;
@property (nonatomic) NSIndexPath *indexPathOfSelectedCell;
@end


@implementation RootViewController

@synthesize fetchedResultsController = _fetchedResultsController;
@synthesize managedObjectContext;
@synthesize selectedObject;

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];
    resultsColor      = [UIColor colorWithRed:0.5  green:0.5  blue:0.5  alpha:1.0];
    staleResultsColor = [UIColor colorWithRed:0.75 green:0.25 blue:0.25 alpha:1.0];
    backgroundEditViews = [NSMutableDictionary dictionaryWithCapacity:5];
    iSageAppDelegate* appDelegate = [UIApplication sharedApplication].delegate;
    self.managedObjectContext = appDelegate.managedObjectContext;
    self.cellsCurrentlyEditing = [NSMutableArray array];
    self.tableView.allowsMultipleSelectionDuringEditing = NO;
}


// Implement viewWillAppear: to do additional setup before the view is presented.
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    
    //present terms of service upon first launch of application
    if (![[NSUserDefaults standardUserDefaults] valueForKey:@"acceptTermsAndConditionsBool"]) {
        NSBundle *bundle = [NSBundle mainBundle];
        NSString *HTMLFile = [bundle pathForResource:@"tos_default" ofType:@"html"];
        NSString *message = [NSString stringWithContentsOfFile:HTMLFile
                                                  usedEncoding:nil
                                                         error:nil];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Terms of Service"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Decline"
                                              otherButtonTitles:@"Accept", nil];
        [alert setTag:705];
        [alert show];//uialertview
    }

    
    
    
    id detail = self.splitViewController.viewControllers[1];
    if ([detail isKindOfClass:[UINavigationController class]]) {
        detail = [((UINavigationController *)detail).viewControllers firstObject];
    }
    EditViewController* detailEditView;
    if ([detail isMemberOfClass:[EditViewController class]]) {
        detailEditView=detail;
    }

    if (detailEditView.selectedObject==NULL) {
        NSIndexPath *firstRow=[NSIndexPath indexPathForRow:0 inSection:0];
        
        
        [self.tableView selectRowAtIndexPath:firstRow animated:YES scrollPosition:UITableViewScrollPositionTop];
        [self tableView:self.tableView didSelectRowAtIndexPath:firstRow];

    }
    
    
    
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = nil;
    if ([[segue identifier] isEqualToString:@"ShowEquationInfo2"])
    {
        indexPath=self.indexPathOfSelectedCell;
        [self prepareEditViewController:segue.destinationViewController fromIndexPath:indexPath];
    }
    
    if ([[segue identifier] isEqualToString:@"NewEquationInfo"])
    {
        [self insertNewObject];
        indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [self prepareEditViewController:segue.destinationViewController fromIndexPath:indexPath];
    }
}

-(void) prepareEditViewController:(EditViewController *)evc fromIndexPath:(NSIndexPath *)indexPath
{
    AlephCell *AlephCell = [self.fetchedResultsController objectAtIndexPath:indexPath];
    evc.delegate = self;
    evc.selectedObject=AlephCell;
    
}

-(void)didUpdateWithJSON:(NSNotification*)notification{
    // NSLog(@"didUpdateWithJSON");
    [self.tableView reloadData];
}


- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
        NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
        if ( ![managedObject isKindOfClass:[AlephCell class]] ) {
            NSLog(@"Something really weird happened");
            return;
        }
        AlephCell * alephCell = (AlephCell*) managedObject;
    
        if ( alephCell.image ) {
    
            // Images are now prescaled
            UIImage * image = [UIImage imageWithData:alephCell.image];
            if ( image.size.width == 70.f ) {  // I know comparing floats is bad...
                cell.imageView.image = image;
            } else {
                // This is only here for old images that weren't prescaled.
                CGSize size = CGSizeMake(70.f, 70.0f);
                UIGraphicsBeginImageContext(size);
                [image drawInRect:CGRectMake(0, 0, size.width, size.height)];
                UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
    
                cell.imageView.image = scaledImage;
            }
    
        } else {
            // In case it's reused from one that had an image
            // TODO: perhaps we should use a different cell cache for those with images...
            cell.imageView.image = nil;
        }
    
        // Empty cells
        if ([alephCell.input isEqualToString:@""]) {
            cell.textLabel.text = @"Tap to edit...";
            cell.textLabel.textColor = staleResultsColor;
        } else {
            cell.textLabel.text = alephCell.input.description;
            cell.textLabel.textColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        }
    
        if ( alephCell.output != nil ) {
            // It doesn't display trailing newlines well, so trim them.
            // This has the unfortunate side effect of trimming newlines off the begining as well.
            cell.detailTextLabel.text = alephCell.output.description;
        } else {
            cell.detailTextLabel.text = @"";
        }
    
        if ( [alephCell isWorking] ) {
            UIActivityIndicatorView * spinner = [[UIActivityIndicatorView alloc]
                                                 initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [spinner startAnimating];
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(spinnerTapped:)];
    
            [spinner addGestureRecognizer:singleTap];
            cell.accessoryView = spinner;
    
    
            // The data is stale, so indicate that
            cell.detailTextLabel.textColor = staleResultsColor;
    
        } else {
    
             //The data is not stale, so indicate that
            cell.detailTextLabel.textColor = resultsColor;
    
            // TODO: my own caching of these images?
            // TODO: should probably cache the buttons instead -- didn't seem to work, maybe you can only use a button once
            // TODO: How to make icons:
            // http://developer.apple.com/library/ios/#documentation/UserExperience/Conceptual/MobileHIG/IconsImages/IconsImages.html%23//apple_ref/doc/uid/TP40006556-CH14-SW1
            // http://mathiasbynens.be/notes/touch-icons -- this is about icons for web apps
            SwipeableCell *temp;
            if ([cell isKindOfClass:[SwipeableCell class]]) {
                temp=(SwipeableCell *) cell;
            }
            switch ( [[alephCell valueForKey:@"status"] intValue] ) {

                case AlephCellStatusHasOutput:
                    temp.button2.backgroundColor=[UIColor greenColor]; //NSLog(@"okay")
                    break;
    
                case AlephCellStatusSyntaxError:
                    // NSLog(@"syntaxError");
                case AlephCellStatusJSONParseError:
                case AlephCellStatusNetworkError:
                    // NSLog(@"networkError");
                    temp.button2.backgroundColor=[UIColor yellowColor];
                    break;
    
                case AlephCellStatusHasInput:
                case AlephCellStatusHasAlephID:
                    // We should be working in these two situations, so if
                    // we're not then it's invalid
                case AlephCellStatusUninitialized:
                default:
                    
                    // NSLog(@"invalid:%@",[alephCell valueForKey:@"status"]);
                    temp.button2.backgroundColor=[UIColor colorWithRed:176 green:0 blue:0 alpha:0];
                    break;
            }
        }
    
}


- (void) accessoryButtonTapped:(id)sender event:(id)event{

    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil) {
        [self tableView:self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

- (void) spinnerTapped:(UIGestureRecognizer *)gestureRecognizer {
    CGPoint currentTouchPosition = [gestureRecognizer locationInView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil) {
        [self tableView:self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void) tableView: (UITableView*)tableView accessoryButtonTappedForRowWithIndexPath: (NSIndexPath*) indexPath {

    // NSLog(@"Accessory button tapped");
    NSManagedObject *managedObject = [self.fetchedResultsController objectAtIndexPath:indexPath];
    if ( ![managedObject isKindOfClass:[AlephCell class]] ) {
        NSLog(@"Something really weird happened.");
        return;
    }
    AlephCell * alephCell = (AlephCell*) managedObject;

    if ( [alephCell isWorking] ) {
        // They tapped on the spinner
        NSLog(@"Requesting cancelled");
        NSString * message = [NSString stringWithFormat:@"The computation is currently running.  You can terminate it if you wish."];

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cancel request"
                                                        message:message
                                                       delegate:self
                                              cancelButtonTitle:@"Continue"
                                              otherButtonTitles:@"Terminate", nil];
        [alert show];
        copyAlephCell = alephCell;
        return;
    }

    switch ([alephCell.status intValue]) {
        case AlephCellStatusSyntaxError: // We may want to copy/email a syntax error to debug it.
        case AlephCellStatusHasOutput: {

            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            [[self shareButtonDelegate] displayShareSheet:self
                                                  forCell:alephCell
                                               fromWhence:cell.accessoryView];
            break;
        }
        case AlephCellStatusNetworkError: {
            NSLog(@"NetworkError retried");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Network Error"
                                                            message:@"Not sure what happened."//[[requestManager networkError] localizedDescription]
                                                           delegate:self
                                                  cancelButtonTitle:@"Fine, whatever"
                                                  otherButtonTitles:@"Retry", nil];
            copyAlephCell = alephCell;
            [alert show];
            break;
        }
        case AlephCellStatusJSONParseError: {
            NSLog(@"JSONParseError error retried");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Response Error"
                                                            message:@"I was unable to understand the response from the server"
                                                           delegate:self
                                                  cancelButtonTitle:@"Fine, whatever"
                                                  otherButtonTitles:@"Retry", nil];
            copyAlephCell = alephCell;
            [alert show];
            break;
        }
        case AlephCellStatusUninitialized:
        default:
            NSLog(@"AlephCellStatusUninitialized:%@",alephCell.status);
            break;
    }
}


#pragma mark -
#pragma mark Add a new object
- (IBAction)insertNewObject:(UIBarButtonItem *)sender {
    [self insertNewObjectWithString:@"" inLanguage:nil];
}

- (void)insertNewObject {
    [self insertNewObjectWithString:@"" inLanguage:nil];
}

- (void)newCellWithDefault:(AlephCell*)initial_value {
    [self insertNewObjectWithString:initial_value.input
                         inLanguage:initial_value.language];
}

- (void)insertNewObjectWithString:(id)initial_value inLanguage:(id)lang {

    // Create a new instance of the entity managed by the fetched results controller.
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    NSEntityDescription *entity = [[self.fetchedResultsController fetchRequest] entity];
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:[entity name] inManagedObjectContext:context];

    // If appropriate, configure the new managed object.
    [newManagedObject setValue:[NSDate date] forKey:@"creation_time"];
    [newManagedObject setValue:nil forKey:@"image"];
    [newManagedObject setValue:[NSNumber numberWithInt:AlephCellStatusUninitialized] forKey:@"status"];

    if ( [initial_value isKindOfClass:[NSString class]] ) {
        [newManagedObject setValue:initial_value forKey:@"input"];
    } else {
        [newManagedObject setValue:@"" forKey:@"input"];
    }

    if ( [lang isKindOfClass:[NSString class]] ) {
        [newManagedObject setValue:lang forKey:@"language"];
    } else {
        [newManagedObject setValue:[[NSUserDefaults standardUserDefaults] stringForKey:@"defaultLanguage"]
                            forKey:@"language"];
    }

    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.

         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle: @"Warning"
                                                        message: @"An error has occured and iSage has to close"
                                                       delegate: nil
                                              cancelButtonTitle:@"Close App"
                                              otherButtonTitles:nil];
        [alert show];
        
        abort();
    }
    [context save:&error];
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated {

    // Prevent new objects being added when in editing mode.
    [super setEditing:(BOOL)editing animated:(BOOL)animated];
    self.navigationItem.rightBarButtonItem.enabled = !editing;
}


#pragma mark -
#pragma mark Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"AlephCell";

    UITableViewCell *cell=[tableView dequeueReusableCellWithIdentifier: CellIdentifier forIndexPath:indexPath];
    
    if ([cell isKindOfClass:[SwipeableCell class]]) {
        SwipeableCell* cell2= (SwipeableCell *)cell;
        if (cell2 == nil) {
            cell2 = [[SwipeableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        cell2.delegate = self;
        if ([self.cellsCurrentlyEditing containsObject:indexPath]) {
            [cell2 openCell];
        }
        cell=cell2;
    } else if ([cell isKindOfClass:[UITableViewCell class]])
    {
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
            
        }
    }
    

    
    
    // Configure the cell.
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}


// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {

    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the managed object for the given index path
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];

        // Save the context.
        NSError *error = nil;
        if (![context save:&error]) {
            /*
             Replace this implementation with code to handle the error appropriately.

             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
        
        NSInteger* row=indexPath.row-1;
        if (row<0) {
            row=0;
        }
        
        NSIndexPath* afterDelete=[NSIndexPath indexPathForRow:row inSection:indexPath.section];
        
        [self.tableView selectRowAtIndexPath:afterDelete animated:YES scrollPosition:UITableViewScrollPositionTop];
        [self tableView:tableView didSelectRowAtIndexPath:afterDelete];
        

        
    } else {
        //NSLog(@"Unhandled editing style! %ld", editingStyle);
    }
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // The table view should not be re-orderable.
    return NO;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([[tableView cellForRowAtIndexPath:indexPath] isKindOfClass:[SwipeableCell class]]) {
        return NO;
    } else if ([[tableView cellForRowAtIndexPath:indexPath] isMemberOfClass:[UITableViewCell class]])
    {
        return YES;
    } else return NO;
}


#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView reloadData];

    id detail = self.splitViewController.viewControllers[1];
    if ([detail isKindOfClass:[UINavigationController class]]) {
        detail = [((UINavigationController *)detail).viewControllers firstObject];
    }
    [self prepareEditViewController:detail fromIndexPath:indexPath];
    [detail updateView];
    
}


- (void)editViewControllerDidFinish:(EditViewController *)controller nextDefault:(NSString*)str inLanguage: lang {
    // The only time this is called is when we want to duplicate one
    [self insertNewObjectWithString:str inLanguage:lang];
}

- (void)saveEditViewForBackground:(EditViewController*)controller {

    NSValue *key = [NSValue valueWithNonretainedObject:controller.selectedObject];
    switch ( [[controller.selectedObject valueForKey:@"status"] intValue] ) {
        case AlephCellStatusHasOutput:
        case AlephCellStatusSyntaxError:
        case AlephCellStatusHasInput:
        case AlephCellStatusJSONParseError:
        case AlephCellStatusNetworkError:
        case AlephCellStatusUninitialized:
        {
            [backgroundEditViews removeObjectForKey:key];
            break;
        }
        case AlephCellStatusHasAlephID:
        default:
            [backgroundEditViews setObject:[NSValue valueWithNonretainedObject:controller]
                                    forKey:key];
            break;
    }
}

#pragma mark -
#pragma mark - shareButtonDelegate

- (ShareButtonDelegate*)shareButtonDelegate
{
    if (!shareButtonDelegate) {
        shareButtonDelegate = [[ShareButtonDelegate alloc] init];
    }
    return shareButtonDelegate;
}


#pragma mark -
#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag==705) {
        
        switch (buttonIndex)
        {
            case 0:
            {
                exit(0);
                break;
            }
            case 1:
            {
                //create the terms of service cookie
                //set tos to accepted
                [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"acceptTermsAndConditionsBool"];
                //set policy accepted date and acception expiration date
                NSDate *today=[[NSDate alloc] init];
                NSDate* expire=[today dateByAddingTimeInterval:60*60*24*365];
                //create the cookie
                NSDictionary *properties =[NSDictionary dictionaryWithObjectsAndKeys:
                                           @"accepted_tos", NSHTTPCookieName,
                                           @"true", NSHTTPCookieValue,
                                           expire.description, NSHTTPCookieExpires,
                                           @"aleph.sagemath.org", NSHTTPCookieDomain,
                                           @"/", NSHTTPCookiePath,
                                           nil];
                NSHTTPCookie* tosCookie=[NSHTTPCookie cookieWithProperties:properties];
                //add the new cookie
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:tosCookie];
                
                properties =[NSDictionary dictionaryWithObjectsAndKeys:
                                           @"accepted_tos", NSHTTPCookieName,
                                           @"true", NSHTTPCookieValue,
                                           expire.description, NSHTTPCookieExpires,
                                           @"sagecell.sagemath.org", NSHTTPCookieDomain,
                                           @"/", NSHTTPCookiePath,
                                           nil];
                NSHTTPCookie* tos2Cookie=[NSHTTPCookie cookieWithProperties:properties];
                //add the new cookie
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookieAcceptPolicy:NSHTTPCookieAcceptPolicyAlways];
                [[NSHTTPCookieStorage sharedHTTPCookieStorage] setCookie:tos2Cookie];
                break;
            }
                
            default:
                break;
        }
//        NSHTTPCookie *cookie;
//        NSHTTPCookieStorage *cookieJar = [NSHTTPCookieStorage sharedHTTPCookieStorage];
//        for (cookie in [cookieJar cookies]) {
//            NSLog(@"%@", cookie);
//        }
        
    }else{
        NSLog(@"button at index %ld",(long)buttonIndex);
        if (copyAlephCell != nil && buttonIndex != [alertView cancelButtonIndex]) {
            // NSLog(@"button named: %@",[alertView buttonTitleAtIndex:buttonIndex]);
            if ( [[alertView buttonTitleAtIndex:buttonIndex] isEqual:@"Terminate"] ) {
                // NSLog(@"Cancelling request");
                [backgroundEditViews removeObjectForKey:[NSValue valueWithNonretainedObject:copyAlephCell]];
                copyAlephCell.isWorking = NO;
                copyAlephCell.status = [NSNumber numberWithInt:AlephCellStatusHasInput];
                [self.tableView reloadData];
                
            } else {
                // TODO: I think this branch isn't used currently
                // NSLog(@"clearing error");
                copyAlephCell.status = [NSNumber numberWithInt:AlephCellStatusHasInput];
                [copyAlephCell clearError];
                // redisplaying the row causes it to also be requested
                [self.tableView reloadData];
            }
        }
        copyAlephCell = nil;
    }
}

#pragma mark -
#pragma mark Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController {

    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }

    // Set up the fetched results controller.
    // Create the fetch request for the entity.
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"AlephCell" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"creation_time" ascending:NO];
    NSArray *sortDescriptors = [[NSArray alloc] initWithObjects:sortDescriptor, nil];
    [fetchRequest setSortDescriptors:sortDescriptors];

    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:@"Root"];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;

    NSError *error = nil;
    if (![_fetchedResultsController performFetch:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.

         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. If it is not possible to recover from the error, display an alert panel that instructs the user to quit the application by pressing the Home button.
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    return _fetchedResultsController;
}


#pragma mark -
#pragma mark Fetched results controller delegate


- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView beginUpdates];
}


- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {

    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath {

    UITableView *tableView = self.tableView;

    switch(type) {

        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;

        case NSFetchedResultsChangeUpdate:
            [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;

        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath]withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}


- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    [self.tableView endUpdates];
}
// Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.

#pragma mark - SwipeableCellDelegate
- (void)deleteButtonActionForPath:(NSIndexPath *)indexPath
{
    // Delete the managed object for the given index path
    NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
    [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
    NSError *error = nil;
    [context save:&error];


}

- (void)buttonTwoActionForItemText:(NSIndexPath *)indexPath
{
    [self tableView:self.tableView accessoryButtonTappedForRowWithIndexPath: indexPath];
    [self closeModal];
}

- (void)showCellDetailsForTableView:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath
{
    id detail = self.splitViewController.viewControllers[1];
    if ([detail isKindOfClass:[UINavigationController class]]) {
        detail = [((UINavigationController *)detail).viewControllers firstObject];
    }
    [self prepareEditViewController:detail fromIndexPath:indexPath];

    [detail updateView];
    self.indexPathOfSelectedCell=indexPath;

}
- (void)closeModal
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)cellDidOpen:(UITableViewCell *)cell
{
    NSIndexPath *currentEditingIndexPath = [self.tableView indexPathForCell:cell];
    [self.cellsCurrentlyEditing addObject:currentEditingIndexPath];
}

- (void)cellDidClose:(UITableViewCell *)cell
{
    [self.cellsCurrentlyEditing removeObject:[self.tableView indexPathForCell:cell]];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    // Relinquish ownership any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    // Relinquish ownership of anything that can be recreated in viewDidLoad or on demand.
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.fetchedResultsController = nil;
}




@end
