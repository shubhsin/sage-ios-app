//
//  SwipeableCell.h
//  iSage
//
//  Created by Daniel Bell on 2014-06-08.
//  Copyright (c) 2014 Sagemath. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol SwipeableCellDelegate <NSObject>
- (void)deleteButtonActionForPath:(NSIndexPath *)indexPath;
- (void)buttonTwoActionForItemText:(NSIndexPath *)indexPath;
- (void)cellDidOpen:(UITableViewCell *)cell;
- (void)cellDidClose:(UITableViewCell *)cell;
- (void)showCellDetailsForTableView:(UITableView *) tableView indexPath:(NSIndexPath *) indexPath;
@end


@interface SwipeableCell : UITableViewCell

//@property (nonatomic, strong) NSString *itemText;
//@property (nonatomic, strong) NSString *itemDetailText;
//@property (nonatomic, strong) UIImage *itemImage;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) id <SwipeableCellDelegate> delegate;
@property (nonatomic, weak) IBOutlet UIButton *button1;
@property (nonatomic, weak) IBOutlet UIButton *button2;

- (void)openCell;

@end
