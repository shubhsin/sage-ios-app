//
//  JSON+iSage.m
//  iSage
//
//  Created by Daniel Bell on 2014-05-22.
//  Copyright (c) 2014 Sagemath. All rights reserved.
//

#import "JSON+iSage.h"
#import <Foundation/NSJSONSerialization.h>


@implementation NSString (JSONSerializing)
// create a JSON string from and NSString
- (id)JSONString
{
    NSMutableArray* testArray=[[NSMutableArray alloc] initWithObjects:self, nil];
    NSError __autoreleasing *error = nil;

    NSData *data = [NSJSONSerialization dataWithJSONObject:testArray
                                                   options:0
                                                     error:&error];

    NSString *testString=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    //remove the brackets from the array
    NSString *prefix = @"["; // string prefix, not needle prefix!
    NSString *suffix = @"]"; // string suffix, not needle suffix!
    NSRange needleRange = NSMakeRange(prefix.length,
                                      testString.length - prefix.length - suffix.length);
    NSString *JSONstring = [testString substringWithRange:needleRange];
    
    
    return JSONstring;
    
}

@end


